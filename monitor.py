#!/usr/bin/env python
# based on code by henryk ploetz
# https://hackaday.io/project/5301-reverse-engineering-a-low-cost-usb-co-monitor/log/17909-all-your-base-are-belong-to-us
from __future__ import print_function
from __future__ import division

try:
    from statistics import median
except ImportError:
    from numpy import median
import fcntl
import time
import threading
try:
    import queue
except ImportError:
    import Queue as queue
import logging
import logging.handlers
import os
import sys
import socket
import yaml
import Adafruit_DHT


def decrypt(key, data):
    cstate = [0x48, 0x74, 0x65, 0x6D, 0x70, 0x39, 0x39, 0x65]
    shuffle = [2, 4, 0, 7, 1, 6, 5, 3]

    phase1 = [0] * 8
    for i, o in enumerate(shuffle):
        phase1[o] = data[i]

    phase2 = [0] * 8
    for i in range(8):
        phase2[i] = phase1[i] ^ key[i]

    phase3 = [0] * 8
    for i in range(8):
        phase3[i] = ((phase2[i] >> 3) | (phase2[(i-1+8) % 8] << 5)) & 0xff

    ctmp = [0] * 8
    for i in range(8):
        ctmp[i] = ((cstate[i] >> 4) | (cstate[i] << 4)) & 0xff

    out = [0] * 8
    for i in range(8):
        out[i] = (0x100 + phase3[i] - ctmp[i]) & 0xff

    return out


def hd(d):
    return " ".join("%02X" % e for e in d)


def now():
    return int(time.time())


def config(cfg_file):
    with open(cfg_file, 'r') as stream:
        return yaml.load(stream)


# publish thread: collect data via queues from DHT and CO2 sensors.
# Average
def publish():
    stamp = now()
    while True:
        # update the timestamp of last publication
        if now() - stamp > CONFIG["publishinterval"]:
            stamp = now()

            lst_co2_tmp = []
            lst_co2_ppm = []
            lst_dht_tmp = []
            lst_dht_hum = []

            while not CO2QUEUE.empty():
                co2_ppm, co2_tmp = CO2QUEUE.get()
                if co2_ppm is not None:
                    lst_co2_ppm.append(co2_ppm)
                if co2_tmp is not None:
                    lst_co2_tmp.append(co2_tmp)

            while not DHTQUEUE.empty():
                dht_hum, dht_tmp = DHTQUEUE.get()
                if dht_hum is not None:
                    lst_dht_hum.append(dht_hum)
                if dht_tmp is not None:
                    lst_dht_tmp.append(dht_tmp)

            # if there a new measurements in the queue calculate their median
            # otherwise republish last known value
            LOG.debug(
                "new measurements: co2 (%d, %d), dht (%d, %d)",
                len(lst_co2_tmp),
                len(lst_co2_ppm),
                len(lst_dht_tmp),
                len(lst_dht_hum)
            )

            # calculate median of temperatures, humidity and CO2 ratio (ppm)
            # from DHT and CO2 sensor since last publication
            co2_tmp_median = median(lst_co2_tmp) if len(lst_co2_tmp) > 0 else float('nan')
            co2_ppm_median = median(lst_co2_ppm) if len(lst_co2_ppm) > 0 else float('nan')
            dht_tmp_median = median(lst_dht_tmp) if len(lst_dht_tmp) > 0 else float('nan')
            dht_hum_median = median(lst_dht_hum) if len(lst_dht_hum) > 0 else float('nan')

            if int(stamp) % 1800 == 0:
                LOG.info(
                    "%d, %3.1f, %3.1f, %3.1f, %4.0f",
                    stamp, dht_hum_median, dht_tmp_median, co2_tmp_median, co2_ppm_median
                )

            sock = socket.socket()
            sock.connect(("localhost", 2003))
            sock.send("tfa.tmp %f %d \n" % (co2_tmp_median, stamp))
            sock.send("tfa.co2 %f %d \n" % (co2_ppm_median, stamp))
            sock.send("dht.tmp %f %d \n" % (dht_tmp_median, stamp))
            sock.send("dht.hum %f %d \n" % (dht_hum_median, stamp))
            sock.close()

        time.sleep(0.1)


def poll_dht_sensor():
    available_dhtsensors = {
        "AM2302": Adafruit_DHT.AM2302,
        "DHT11": Adafruit_DHT.DHT11,
        "DHT22": Adafruit_DHT.DHT22
    }

    while True:
        humid, tmp = Adafruit_DHT.read_retry(
            available_dhtsensors[CONFIG["dhttype"]],
            CONFIG["dhtdatapin"]
        )

        # push measurement to queue -> to be read from the publish thread
        DHTQUEUE.put((humid, tmp))

        time.sleep(0.1)


def poll_co2_sensor():
    values = {}
    try:
        fp_hiddev = open(CONFIG["co2hiddev"], "a+b", 0)
    except IOError:
        LOG.error("Cant open %s", CONFIG["co2hiddev"])
        return

    hidiocsfeature_9 = 0xC0094806
    key = [0xc4, 0xc6, 0xc0, 0x92, 0x40, 0x23, 0xdc, 0x96]
    set_report = "\x00" + "".join(chr(e) for e in key)
    try:
        fcntl.ioctl(fp_hiddev, hidiocsfeature_9, set_report)
    except OSError as error:
        LOG.error("fnctl.ioctl error: %s", error)
        return

    while True:
        data = list(ord(e) for e in fp_hiddev.read(8))
        decrypted = decrypt(key, data)

        if decrypted[4] != 0x0d or (sum(decrypted[:3]) & 0xff) != decrypted[3]:
            LOG.error(hd(data), " => ", hd(decrypted), "Checksum error")
        else:
            values[decrypted[0]] = decrypted[1] << 8 | decrypted[2]

            if (0x50 in values) and (0x42 in values):
                co2 = values[0x50]
                tmp = (values[0x42]/16.0-273.15)

                # push measurement to queue -> to be read from the publish thread
                CO2QUEUE.put((co2, tmp))

        time.sleep(0.1)


def main():
    LOG.setLevel(logging.DEBUG)
    fhandler = logging.handlers.RotatingFileHandler(
        LOGFILE, maxBytes=10 * 1024 * 1024, backupCount=16
    )
    fhandler.setFormatter(
        logging.Formatter('%(asctime)s - %(threadName)s - %(levelname)s - %(message)s')
    )
    fhandler.setLevel(logging.INFO)
    LOG.addHandler(fhandler)
    LOG.info("=== Starting Monitor ===")
    # start the publication thread
    try:
        pub_thread = threading.Thread(name="publication", target=publish)
        pub_thread.setDaemon(True)
        pub_thread.start()
        LOG.info("... started publication thread.")
    except RuntimeError:
        LOG.error("Error: unable to start CO2 sensor thread")

    # prepare co2 sensor if co2enable set true in config
    if CONFIG["co2enable"]:
        # start co2 sensor thread
        try:
            co2_thread = threading.Thread(name="co2", target=poll_co2_sensor)
            co2_thread.setDaemon(True)
            co2_thread.start()
            LOG.info("... started co2 thread.")
        except RuntimeError:
            LOG.error("Error: unable to start CO2 sensor thread")

    # prepare humidity sensor if dhtenable set true in config
    if CONFIG["dhtenable"]:
        # start dht sensor thread
        try:
            dht_thread = threading.Thread(name="dht", target=poll_dht_sensor)
            dht_thread.setDaemon(True)
            dht_thread.start()
            LOG.info("... started dht thread.")
        except RuntimeError:
            LOG.error("Error: unable to start DHT sensor thread")
    LOG.info("done")
    while True:
        time.sleep(10)


if __name__ == "__main__":
    LOGFILE = os.path.expanduser(u"~/.office_weather/office.log")
    if not os.path.exists(os.path.dirname(LOGFILE)):
        os.makedirs(os.path.dirname(LOGFILE))
    LOG = logging.getLogger(__name__)

    CONFIG = config(sys.argv[1])

    CO2QUEUE = queue.Queue()
    DHTQUEUE = queue.Queue()

    main()
