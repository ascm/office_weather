# what & why?

Measuring CO2 and Temperature at NFF.
> People are sensitive to high levels of Co2 or uncomfortably hot work environments, so we want to
have some numbers.

Based on [wooga/office_weather](https://github.com/wooga/office_weather).

# requirements

## hardware

1) [TFA-Dostmann AirControl Mini CO2 Messgerät](http://www.amazon.de/dp/B00TH3OW4Q) -- 80 euro

2) [Raspberry PI 2 Model B](http://www.amazon.de/dp/B00T2U7R7I) -- 40 euro

3) [DHT22/AM2302 sensor](http://www.amazon.de/s/ref=nb_sb_noss?field-keywords=DHT22) -- 10 euro

4) case, 5v power supply, microSD card

## software

1) [Librato](https://www.librato.com) account for posting the data to.

2) download [Raspbian](https://www.raspberrypi.org/downloads/) and [install it on the microSD](https://www.raspberrypi.org/documentation/installation/installing-images/README.md). We used [this version](https://github.com/wooga/office_weather/blob/0da94b4255494ecbcf993ec592988503c6c72629/.gitignore#L2) of raspbian.

3) See [Adafruit DHT Tutorial](https://learn.adafruit.com/dht-humidity-sensing-on-raspberry-pi-with-gdocs-logging) for the DHT22/AM2302 sensor

# installation on the raspberry

0) Boot the raspberry with the raspbian. You'll need a USB-keyboard, monitor and ethernet for this initial boot. After overcoming the initial configuration screen, you can login into the box using ssh.

1) install python libs and build tools
```
sudo apt-get install python-pip python-dev python-openssl libyaml-dev build-essential
sudo pip install librato-metrics
sudo pip install pyyaml
```

2) Install Adafruit DHT python libs
```
git clone https://github.com/adafruit/Adafruit_Python_DHT.git
cd Adafruit_Python_DHT
sudo python setup.py install
```

3) Connect the DHT22/AM2302 to the Pi's GPIO (+ -> Pin 1 (V3.3), DATA -> Pin 7 (GPIO04), GND -> Pin 6 (GND)).
Test correct wiring:
```
cd examples
sudo ./AdafruitDHT.py 2302 4
```

4) create `config.yaml` with [libratro](https://www.librato.com) credentials:
```
user: your.librato.user.name@some.email.com
token: abc123...
prefix: office.floor3
```

We use [librato](https://www.librato.com) to graph our weather, so you'll need to modify that if you using another service.

5) fix socket permissions
```
sudo chmod a+rw /dev/hidraw0
```

6) run the script
```
./monitor.py /dev/hidraw0
```

6) run on startup

To get everything working on startup, need to add one crontab for root (to read from the DHT22 sensor, currently one needs to be root):

```
SHELL=/bin/bash
* * * * * if ( ! pidof python ) ; then cd /home/pi/office_weather/; /usr/bin/python monitor.py > /dev/null 2>&1 ; fi
```

This assumes that only one python is running on the box...

# credits

based on code by [henryk ploetz](https://hackaday.io/project/5301-reverse-engineering-a-low-cost-usb-co-monitor/log/17909-all-your-base-are-belong-to-us)

# license

[MIT](http://opensource.org/licenses/MIT)
